<?php

namespace App\Http\Controllers;

use App\Settings;
use App\Markup;

use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function index()
    {
        $settings = Settings::all();
        $markups = Markup::all();
        return view('crm.settings.index', ['settings' =>$settings, 'markups' =>$markups]);
    }

    public function setKidSeatsCost(Request $request)
    {
        $kidSeatsCost = Settings::where('name', 'kidSeatsCost')->first();
        $kidSeatsCost->value = $request->price;
        $kidSeatsCost->save();
        return redirect()->route('settings.index');
    }

}

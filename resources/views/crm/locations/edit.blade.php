@extends('layouts.crm')
@section('pageTitle', 'Edit Location '. $location->name)

@section('content')
    <div class="row">
        <div class="col-xl-12 col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit location</h3>
                </div>
                <div class="row">
                    <form role="form" action="{{route('locations.update', ['id'=>$location->id])}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        {{method_field('PUT')}}
                        <div class="box-body">
                            <div class="form-group @if($errors->has('name'))has-error @endif">
                                <label for="name" class="col-md-2 cols-sm-12 control-label">Name</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="name" id="name"
                                           value = "{{$location->name}}"
                                           class="form-control" placeholder="Name" required>
                                    @foreach ($errors->get('name') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 cols-sm-12 control-label" for="is_airport">Is it airport?</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="checkbox" class="form-check-input" id="is_airport" name="is_airport" value="1"
                                    @if($location->is_airport) checked @endif
                                    >
                                </div>
                            </div>
                        </div>
                        <div class="box-footer" style="background: none">
                            <div class="form-group col-lg-2 col-lg-offset-7 col-sm-10 col-sm-offset-1 col-xs-12">
                                <button type="submit"
                                        class=" form-group btn input-block-level form-control btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
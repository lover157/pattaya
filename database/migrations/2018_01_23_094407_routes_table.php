<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('id_pick_up')->nullable()->unsigned();
            $table->foreign('id_pick_up')->references('id')->on('locations')->onDelete('cascade');

            $table->integer('id_drop_off')->nullable()->unsigned();
            $table->foreign('id_drop_off')->references('id')->on('locations')->onDelete('cascade');

            $table->string('travels_time');
            $table->integer('mileage');
            $table->integer('commission');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routes');
    }
}

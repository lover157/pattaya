<?php

namespace App\Http\Controllers;

use App\Car;
use App\Location;
use Illuminate\Http\Request;

use View;

class SiteController extends Controller
{
    public function home()
    {
        return view('site.home');
    }

    public function vehicles()
    {
        $locations = Location::all();
        $cars = Car::all();
        return view('site.vehicles', ['locations' => $locations, 'cars' => $cars]);

    }

    public function reservation()
    {
        $locations = Location::all();
        $cars = Car::all();
        return view('site.reservation', ['locations' => $locations, 'cars' => $cars]);

    }
}

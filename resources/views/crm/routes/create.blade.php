@extends('layouts.crm')
@section('pageTitle', 'Add new Route')

@section('content')
    <div class="row">
        <div class="col-xl-12 col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Add new Route</h3>
                </div>
                <div class="row">
                    <form role="form" action="{{route('routes.store')}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}

                        <div class="box-body">
                            <div class="form-group @if($errors->has('id_pick_up'))has-error @endif">
                                <label for="id_pick_up" class="col-md-2 cols-sm-12 control-label">From</label>
                                <div class="col-md-7 col-sm-12">
                                    <select name="id_pick_up" id="id_pick_up" class="form-control" required>
                                        @foreach($locations as $location)
                                            <option value="{{$location->id}}">{{$location->name}}</option>
                                        @endforeach
                                    </select>
                                    @foreach ($errors->get('id_pick_up') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('id_drop_off'))has-error @endif">
                                <label for="id_drop_off" class="col-md-2 cols-sm-12 control-label">To</label>
                                <div class="col-md-7 col-sm-12">
                                    <select name="id_drop_off" id="id_drop_off" class="form-control" required>
                                        @foreach($locations as $location)
                                            <option value="{{$location->id}}">{{$location->name}}</option>
                                        @endforeach
                                    </select>
                                    @foreach ($errors->get('id_drop_off') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('travels_time'))has-error @endif">
                                <label for="travels_time" class="col-md-2 cols-sm-12 control-label">Travel's time</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="travels_time" id="travels_time" required
                                           value = "{{Input::old('travels_time')}}"
                                           class="form-control" placeholder="3,5 H">
                                    @foreach ($errors->get('travels_time') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('mileage'))has-error @endif">
                                <label for="mileage" class="col-md-2 cols-sm-12 control-label">Mileage</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="mileage" id="mileage" required
                                           value = "{{Input::old('mileage')}}"
                                           class="form-control" placeholder="150">
                                    @foreach ($errors->get('mileage') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('commission'))has-error @endif">
                                <label for="commission" class="col-md-2 cols-sm-12 control-label">Commission</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="commission" id="commission" required
                                           value = "{{Input::old('commission')}}"
                                           class="form-control" placeholder="150">
                                    @foreach ($errors->get('commission') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>

                            <h4 class="box-title" style="margin-left: 15px">Cars Prices:</h4>

                            @foreach($cars as $car)
                                <div class="form-group @if($errors->has('car_'.$car->id))has-error @endif">
                                    <label for="car_{{$car->id}}" class="col-md-2 cols-sm-12 control-label">{{$car->name}}</label>
                                    <div class="col-md-7 col-sm-12">
                                        <input type="text" name="car_{{$car->id}}" id="car_{{$car->id}}" required
                                               value = "{{Input::old('car_'.$car->id)}}"
                                               class="form-control" placeholder="1500">
                                        @foreach ($errors->get('car_'.$car->id) as $message)
                                            <span class="help-block">{{ $message }}</span>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach

                        </div>


                        <div class="box-footer" style="background: none">
                            <div class="form-group col-lg-2 col-lg-offset-7 col-sm-10 col-sm-offset-1 col-xs-12">
                                <button type="submit"
                                        class=" form-group btn input-block-level form-control btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    public function prices()
    {
        return $this->hasMany('App\Price', 'id_route');
    }

    public function location_from(){
        return $this->belongsTo('App\Location', 'id_pick_up');
    }

    public function location_to(){
        return $this->belongsTo('App\Location', 'id_drop_off');
    }
}

<?php

namespace App\Http\Controllers;
use App\Car;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use function redirect;

class CarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $cars = Car::all();
        return view('crm.cars.index', ['cars'=>$cars]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('crm.cars.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        //TODO Validate -> Store cars

        $car = new Car();
        $car->name = $request->name;
        $car->about_1 = $request->about_1;
        $car->about_2 = $request->about_2;

        if ($request->file('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->save(public_path('/upload/' . $imageName));
            Image::make($image)->resize(80, 23)->save(public_path('/upload/small_' . $imageName));
            $car->image = $imageName;
        }
        $car->save();
        return redirect()->route('cars.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //TODO Car SHOW
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $car = Car::findOrFail($id);
        return view('crm.cars.edit', ['car'=>$car]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        //TODO Validate -> Upadte cars

        $car = Car::findOrFail($id);
        $car->name = $request->name;
        $car->about_1 = $request->about_1;
        $car->about_2 = $request->about_2;

        if ($request->file('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->save(public_path('/upload/' . $imageName));
            Image::make($image)->resize(80, 23)->save(public_path('/upload/small_' . $imageName));
            $car->image = $imageName;
        }
        $car->save();
        return redirect()->route('cars.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $car = Car::findOrFail($request->id);
        $car->delete();

        return redirect()->route('cars.index');
    }
}

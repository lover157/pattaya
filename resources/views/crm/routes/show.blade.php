@extends('layouts.crm')
@section('pageTitle', 'Routes view')

@section('content')
    <div class="row">
        <div class="col-xl-12 col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Add new Route</h3>
                </div>
                From:
                {{$locationFrom['name']}} <br/>
                To:
                {{$locationTo['name']}}

                <div class="row">

                </div>
            </div>

        </div>
    </div>
@endsection
@extends('layouts.crm')
@section('pageTitle', 'Car:'.$car->name)

@section('content')
    <div class="row">
        <div class="col-xl-12 col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Add new car</h3>
                </div>
                <div class="row">
                    <form role="form" action="{{route('cars.update', ['id'=>$car->id])}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        {{method_field('PUT')}}
                        <div class="box-body">
                            <div class="form-group @if($errors->has('name'))has-error @endif">
                                <label for="name" class="col-md-3 cols-sm-12 control-label">Name</label>
                                <div class="col-md-7 col-sm-12 el-input">
                                    <input type="text" name="name" id="name"
                                           value = "{{$car->name}}"
                                           class="form-control" placeholder="Name">
                                    @foreach ($errors->get('name') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('about_1'))has-error @endif">
                                <label for="name" class="col-md-3 cols-sm-12 control-label">About (line 1)</label>
                                <div class="col-md-7 col-sm-12 el-input">
                                    <input type="text" name="about_1" id="about_1"
                                           value = "{{$car->about_1}}"
                                           class="form-control" placeholder="About (line 1)">
                                    @foreach ($errors->get('about_1') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('about_2'))has-error @endif">
                                <label for="name" class="col-md-3 cols-sm-12 control-label">About (line 2)</label>
                                <div class="col-md-7 col-sm-12 el-input">
                                    <input type="text" name="about_2" id="about_2"
                                           value = "{{$car->about_2}}"
                                           class="form-control" placeholder="About (line 2)">
                                    @foreach ($errors->get('about_2') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="image" class="col-md-3 cols-sm-12 control-label">image</label>
                                <div class="col-md-7 col-sm-12 el-input">
                                    <input type="file" name="image" id="image"
                                           class="form-control">
                                </div>
                            </div>


                        </div>
                        <div class="box-footer" style="background: none">
                            <div class="form-group col-lg-2 col-lg-offset-8 col-sm-10 col-sm-offset-1 col-xs-12">
                                <button type="submit"
                                        class=" form-group btn input-block-level form-control btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
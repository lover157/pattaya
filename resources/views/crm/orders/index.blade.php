@extends('layouts.crm')
@section('pageTitle', 'Orders')

@section('styles')
    <link href="{{ asset('plugins/datepicker/datepicker3.css') }}" rel="stylesheet">
@endsection

@section('content')

    <div class="row">
        <div class="col-xs-6">
            <a href="{{route('orders.create')}}" class="btn btn-success" style="margin-bottom: 10px">
                <i class="fa fa-plus" aria-hidden="true" title="Add new Order"></i> Add new Order</a>
            <a href="#filtertoogle" class="btn btn-primary" style="margin-bottom: 10px" data-toggle="collapse"
               role="button"
               aria-expanded="false" aria-controls="filtertoogle">
                <i class="fa fa-search" aria-hidden="true" title="Show Filter"></i> Show Filter</a>
        </div>
    </div>

    <?php
    $collapse = 'collapse';
    if (Request::get('filter') == 'y')
        $collapse = ''
    ?>

    <div class="row {{$collapse}}" id="filtertoogle">
        <div class="col-xs-12">
            <form action="" method="GET">
                <input type="hidden" value="y" name="filter">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Filter:</h3>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                <h4>Dates</h4>
                                <div class="form-group">
                                    <button class="btn btn-default btn-block"
                                            onclick="setToday(); return false;"
                                            role="button">
                                        Today
                                    </button>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <input type="text" class="pickup-arrival-date form-control" id="date_from"
                                               placeholder="Date from"
                                               name="date_from" data-date-format="d/m/yyyy"
                                               value="{{Request::get('date_from')}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <input type="text" class="form-control pickup-arrival-date" id="date_to"
                                               placeholder="Date to"
                                               name="date_to" data-date-format="d/m/yyyy"
                                               value="{{Request::get('date_to')}}">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-12">
                                <h4>Routes</h4>
                                <div class="form-group">
                                    <select name="location_from" class="form-control">
                                        <option value="0">Select from</option>
                                        @foreach($locations as $location)
                                            <option value="{{$location['id']}}"
                                                    @if(Request::get('location_from') == $location['id']) selected @endif
                                            >{{$location['name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select name="location_to" class="form-control">
                                        <option value="0">Select To</option>
                                        @foreach($locations as $location)
                                            <option value="{{$location['id']}}"
                                                    @if(Request::get('location_to') == $location['id']) selected @endif
                                            >{{$location['name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-12">
                                <h4>Other</h4>
                                <div class="form-group">
                                    <select name="id_status" class="form-control">
                                        <option value="0">Select Status</option>
                                        @foreach($statuses as $status)
                                            <option value="{{$status['id']}}"
                                                    @if(Request::get('id_status') == $status['id']) selected @endif
                                            >{{$status['name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="box-footer">
                        <a href="{{route('orders.index')}}"
                           class="col-md-3 col-md-offset-2 col-xs-12 btn btn-danger"
                        >Reset</a>
                        <input type="submit" class="col-md-3 col-md-offset-2 col-xs-12 btn btn-primary"
                               value="Filter"/>
                    </div>

                </div>
            </form>
        </div>
    </div>



    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Orders:</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Date</th>
                            <th>Client name</th>
                            <th>From</th>
                            <th>To</th>
                            <th>Vehicle</th>
                            <th>VIP</th>
                            <th>Price</th>
                            <th>Agent</th>
                            <th>Status</th>
                            <th>%</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>{{$order->id}}</td>
                                <td>{{$order->date}}</td>
                                <td>
                                    @if($order->id_client)
                                        {{$clients[$order->id_client]['name']}}
                                    @endif
                                </td>
                                <td>
                                    @if($order->id_route)
                                        {{$locations[$routes[$order->id_route]['id_pick_up']]['name']}}
                                    @endif
                                </td>
                                <td>
                                    @if($order->id_route)
                                        {{$locations[$routes[$order->id_route]['id_drop_off']]['name']}}
                                    @endif
                                </td>
                                <td>
                                    @if($order->id_car)
                                        {{$cars[$order->id_car]['name']}}
                                    @endif
                                </td>
                                <td>
                                    @if($order->vip)
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    @endif
                                </td>
                                <td>{{$order->total_price}}</td>
                                <td>
                                    @if($order->id_agent)
                                        {{$agents[$order->id_agent]['name']}}
                                    @endif
                                </td>
                                <td>
                                    @if($order->id_status)
                                        {{$statuses[$order->id_status]['name']}}
                                    @endif
                                </td>
                                <td>{{$order->commission}}</td>
                                <td>
                                    <a href="{{route('orders.show', ['id'=>$order->id])}}" title="Show">
                                        <i class="fa fa-2x fa-eye"></i>
                                    </a>
                                    <a href="{{route('orders.edit', ['id'=>$order->id])}}" title="Edit">
                                        <i class="fa fa-2x fa-pencil"></i>
                                    </a>
                                    <a href="#" role="button" title="Delete" onclick="confirmDeleteOrder({{$order->id}})"><i class="fa fa-2x fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>id</th>
                            <th>Date</th>
                            <th>Client name</th>
                            <th>From</th>
                            <th>To</th>
                            <th>Vehicle</th>
                            <th>VIP</th>
                            <th>Price</th>
                            <th>Agent</th>
                            <th>Status</th>
                            <th>%</th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="box-footer text-center">
                    {!! $orders->appends(Input::except('page'))->links() !!}
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="modal modal-default" id="deleteOrder" tabindex="-1" role="dialog" aria-labelledby="modalDeleteOrder"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Delete order</h4>
                </div>
                <form role="form" action="{{route('orders.destroy')}}" method="POST"
                      enctype="multipart/form-data"
                      id="formUserDelete">
                    <div class="modal-body">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                        <input type="hidden" value="" id="orderID" name="orderID">
                        <p>Confirm delete order </p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn pull-left" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>

    <script type="text/javascript">
        $('#date_from').datepicker({
            autoclose: true
        });
        $('#date_to').datepicker({
            autoclose: true
        });


        function setToday() {
            var date = moment();
            // date.tz('Asia/Bangkok').format('D/M/YYYY');
            $('#date_from').val(moment().tz('Asia/Bangkok').format('D/M/YYYY'));
            $('#date_to').val(moment().tz('Asia/Bangkok').format('D/M/YYYY'));
        }

        function confirmDeleteOrder(id) {
            $("#orderID").val(id);
            $('#deleteOrder').modal('show');
        }
    </script>
@endsection

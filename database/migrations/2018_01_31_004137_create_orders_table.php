<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('id_route')->unsigned()->nullable();
            $table->foreign('id_route')->references('id')->on('routes')->onDelete('set null');

            $table->integer('id_client')->unsigned()->nullable();
            $table->foreign('id_client')->references('id')->on('clients')->onDelete('set null');

            $table->integer('id_car')->unsigned()->nullable();
            $table->foreign('id_car')->references('id')->on('cars')->onDelete('set null');

            $table->integer('id_agent')->unsigned()->nullable();
            $table->foreign('id_agent')->references('id')->on('agents')->onDelete('set null');
            $table->dateTime('date');

            $table->string('pick_up_address')->nullable();
            $table->string('drop_off_address')->nullable();
            $table->string('flight_num')->nullable();

            $table->integer('count_adults')->default(0);
            $table->integer('count_kids')->default(0);
            $table->integer('count_kids_seat')->default(0);
            $table->string('seats_info')->nullable();

            $table->integer('count_luggage')->default(0);
            $table->string('luggage_info')->nullable();

            $table->boolean('vip')->default('0');
            $table->string('total_price')->nullable();
            $table->string('commission')->nullable();

            $table->text('special_request')->nullable();

            $table->integer('id_status')->nullable()->unsigned();
            $table->foreign('id_status')->references('id')->on('statuses')->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

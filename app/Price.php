<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    public function route()
    {
        return $this->belongsTo('App\Route');
    }

    public function cars()
    {
        return $this->belongsTo('App\Car');
    }
}

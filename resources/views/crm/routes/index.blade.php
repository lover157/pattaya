@extends('layouts.crm')
@section('pageTitle', 'Routes')

@section('content')
    <a href="{{route('routes.create')}}" class="btn btn-success" style="margin-bottom: 10px">
        <i class="fa fa-plus" aria-hidden="true" title="Add new Route"></i> Add new Route</a>
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Routes:</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>From</th>
                            <th>To</th>
                            <th>Travel's time</th>
                            <th>Mileage</th>
                            <th>Commission</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($routes as $route)
                            <tr>
                                <td>{{$route->id}}</td>

                                <td>{{$locations[$route->id_pick_up]['name']}}</td>
                                <td>{{$locations[$route->id_drop_off]['name']}}</td>
                                <td>{{$route->travels_time}}</td>
                                <td>{{$route->mileage}}</td>
                                <td>{{$route->commission}}</td>
                                <td>
                                    <a href="{{route('routes.edit', ['id'=>$route->id])}}" title="Edit">
                                        <i class="fa fa-2x fa-pencil"></i>
                                    </a>
                                    <a href="#" role="button" onclick="confirmDeleteRoute('{{$route->id}}')" title="delete"><i class="fa fa-2x fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>id</th>
                            <th>From</th>
                            <th>To</th>
                            <th>Travel's time</th>
                            <th>Mileage</th>
                            <th>Commission</th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="modal modal-default" id="deleteRoute" tabindex="-1" role="dialog" aria-labelledby="modalDeleteRoute"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Delete route</h4>
                </div>
                <form role="form" action="{{route('routes.destroy')}}" method="POST"
                      enctype="multipart/form-data"
                      id="formUserDelete">
                    <div class="modal-body">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                        <input type="hidden" value="" id="routeID" name="id">
                        <p>Confirm delete route </p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn pull-left" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


@endsection


@section('scripts')
    <script type="text/javascript">
        function confirmDeleteRoute(id) {
            $("#routeID").val(id);
            $('#deleteRoute').modal('show');
        }
    </script>
@endsection

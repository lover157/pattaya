@extends('layouts.crm')
@section('pageTitle', 'Agents')

@section('content')
    <a href="{{route('agents.create')}}" class="btn btn-success" style="margin-bottom: 10px">
        <i class="fa fa-plus" aria-hidden="true" title="Add new Agent"></i> Add new Agent</a>
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Agents:</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Info</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($agents as $agent)
                            <tr>
                                <td>{{$agent->id}}</td>
                                <td>{{$agent->name}}</td>
                                <td>{{$agent->phone}}</td>
                                <td>{{$agent->info}}</td>
                                <td>
                                    <a href="{{route('agents.edit', ['id'=>$agent->id])}}" title="Edit"><i
                                                class="fa fa-2x fa-pencil"></i></a>
                                    <a href="#" role="button"
                                       onclick="confirmDeleteAgent('{{$agent->id}}', '{{$agent->name}}')"
                                       title="delete"><i class="fa fa-2x fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>id</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Info</th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="modal modal-default" id="deleteAgent" tabindex="-1" role="dialog" aria-labelledby="modalDeleteAgent"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Delete agent</h4>
                </div>
                <form role="form" action="{{route('agents.destroy')}}" method="POST"
                      enctype="multipart/form-data"
                      id="formUserDelete">
                    <div class="modal-body">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                        <input type="hidden" value="" id="agentID" name="id">
                        <p>Confirm delete agent <span class="text-bold" id="agentName"></span></p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn pull-left" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


@endsection


@section('scripts')
    <script type="text/javascript">
        function confirmDeleteAgent(id, name) {
            $("#agentName").text(name);
            $("#agentID").val(id);
            $('#deleteAgent').modal('show');
        }
    </script>
@endsection
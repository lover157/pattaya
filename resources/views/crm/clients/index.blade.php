@extends('layouts.crm')
@section('pageTitle', 'Clients')

@section('content')
    <a href="{{route('clients.create')}}" class="btn btn-success" style="margin-bottom: 10px">
        <i class="fa fa-plus" aria-hidden="true" title="Add new Clients"></i> Add new Client</a>
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Clients:</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($clients as $client)
                            <tr>
                                <td>{{$client->id}}</td>
                                <td>{{$client->name}}</td>
                                <td>{{$client->email}}</td>
                                <td>{{$client->phone}}</td>
                                <td>
                                    <a href="{{route('clients.edit', ['id'=>$client->id])}}" title="Edit"><i
                                                class="fa fa-2x fa-pencil"></i></a>
                                    <a href="#" role="button" onclick="confirmDeleteClient('{{$client->id}}', '{{$client->name}}')" title="delete"><i class="fa fa-2x fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>id</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="modal modal-default" id="deleteClient" tabindex="-1" role="dialog" aria-labelledby="modalDeleteLocation"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Delete client</h4>
                </div>
                <form role="form" action="{{route('clients.destroy')}}" method="POST"
                      enctype="multipart/form-data"
                      id="formUserDelete">
                    <div class="modal-body">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                        <input type="hidden" value="" id="clientID" name="id">
                        <p>Confirm delete client <span class="text-bold" id="clientName"></span></p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn pull-left" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


@endsection


@section('scripts')
    <script type="text/javascript">
        function confirmDeleteClient(id, name) {
            $("#clientName").text(name);
            $("#clientID").val(id);
            $('#deleteClient').modal('show');
        }
    </script>
@endsection

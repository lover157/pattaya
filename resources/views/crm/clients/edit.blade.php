@extends('layouts.crm')
@section('pageTitle', 'Client')

@section('content')
    <div class="row">
        <div class="col-xl-12 col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Add new Client</h3>
                </div>
                <div class="row">
                    <form role="form" action="{{route('clients.update', ['id'=>$client->id])}}" method="POST"
                          enctype="multipart/form-data">
                        {{csrf_field()}}
                        {{method_field('PUT')}}
                        <div class="box-body">
                            <div class="form-group @if($errors->has('name'))has-error @endif">
                                <label for="name" class="col-md-3 cols-sm-12 control-label">Name</label>
                                <div class="col-md-7 col-sm-12 el-input">
                                    <input type="text" name="name" id="name"
                                           value = "{{$client->name}}"
                                           class="form-control" placeholder="Name"
                                    required>
                                    @foreach ($errors->get('name') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('email'))has-error @endif">
                                <label for="email" class="col-md-3 cols-sm-12 control-label">Email</label>
                                <div class="col-md-7 col-sm-12 el-input">
                                    <input type="email" name="email" id="email"
                                           value = "{{$client->email}}"
                                           class="form-control" placeholder="email@example.com"
                                    required>
                                    @foreach ($errors->get('email') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('phone'))has-error @endif">
                                <label for="phone" class="col-md-3 cols-sm-12 control-label">Phone</label>
                                <div class="col-md-7 col-sm-12 el-input">
                                    <input type="phone" name="phone" id="phone"
                                           value = "{{$client->phone}}"
                                           class="form-control" placeholder="+1(123)1234567"
                                    required>
                                    @foreach ($errors->get('phone') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>



                        </div>
                        <div class="box-footer" style="background: none">
                            <div class="form-group col-lg-2 col-lg-offset-8 col-sm-10 col-sm-offset-1 col-xs-12">
                                <button type="submit"
                                        class=" form-group btn input-block-level form-control btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
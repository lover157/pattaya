@extends('layouts.site')
@section('pageTitle', 'Vehicles')

@section('content')
    <!-- OUR VEHICLES -->
    <section class="our-vehicles">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <div class="our-vehicles-inforamation">
                        <ul>
                            <li>PRIVATE TRANSFER SERVICE</li>
                            <li>Suvarnabhumi Airport</li>
                            <li>Don Mueang Airport</li>
                            <li>U-Topao Airport</li>
                            <li>Pattaya</li>
                            <li>Jomtien</li>
                            <li>Chonburi</li>
                            <li>Koh Samet Island</li>
                            <li>Koh Chang Island</li>
                            <li>Rayong</li>
                            <li>Bangkok City</li>
                            <li>Hua Hin</li>
                            <li>Cha Am</li>
                            <li>Kanchanaburi</li>
                            <li>Autthaya</li>
                        </ul>
                        <ul>
                            <li>BOOKING TERMS</li>
                            <li>no deposit required</li>
                            <li>free cancelation</li>
                            <li>all inclusive</li>
                            <li>payment to driver upon arrival</li>
                        </ul>
                        <ul>
                            <li>BENEFITS</li>
                            <li>hassle free service</li>
                            <li>always on time</li>
                            <li>new clean vehicles</li>
                            <li>professional drivers</li>
                        </ul>
                    </div>

                    <div class="our-vehicles-quote-trip">
                        <p class="our-vehicles-quote-trip-label">QUOTE MY TRIP</p>
                        <form class="our-vehicles-quote-trip-form" action="">
                            <label for="">Pick me up from</label>
                            <br>
                            <select name="id_pick_up" id="id_pick_up" onchange="getCosts()">
                                <option value="0" selected>Pick up locaton</option>
                                @foreach($locations as $location)
                                    <option value="{{$location->id}}">{{$location->name}}</option>
                                @endforeach
                            </select>
                            <br>
                            <label for="">I am traveling to</label>
                            <br>
                            <select name="id_drop_off" id="id_drop_off" onchange="getCosts()">
                                <option value="0" selected>Drop off location</option>
                                @foreach($locations as $location)
                                    <option value="{{$location->id}}">{{$location->name}}</option>
                                @endforeach
                            </select>
                            <br>
                            <label for="">Select date</label>
                            <br>
                            <input class="pick-up-date-vehicles datePickerVehicles" type="text" name="date" data-timepicker="true"
                                   placeholder="01.01.2018">
                            <br>
                            <label for="">Rates</label>
                            <div class="images-radio">
                                @foreach($cars as $car)
                                    <label>
                                        <input type="radio" name="id_car" id="id_car" value="{{$car->id}}" onchange="getCosts()"/>
                                        <div>
                                            <img src="upload/small_{{$car->image}}">
                                            <p><span id="price_car_{{$car->id}}"></span> baht</p>
                                        </div>
                                    </label>
                                    <br>
                                @endforeach
                            </div>
                            <div class="form-location-block">
                                <img src="img/location.png" alt="">
                                <p><span id="mileage">_ _ _</span>km</p>
                            </div>
                            <div class="form-duration-block">
                                <img src="img/duration.png" alt="">
                                <p><span id="travels_time">_ _ _</span>h</p>
                            </div>
                            <button class="book-your-trip-btn">BOOK YOUR TRIP</button>
                        </form>
                    </div>

                    <div class="list-our-vehicles">
                        <p class="list-our-vehicles-label">OUR VEHICLES</p>
                        @foreach($cars as $car)
                        <div class="camry-vehicle our-vehicles-part">
                            <img src="upload/{{$car->image}}" alt="">
                            <p>{{$car->name}}</p>
                            <p>{{$car->about_1}} <br>{{$car->about_2}}</p>
                        </div>
                        @endforeach
                    </div>


                </div>
            </div>
        </div>
    </section>

    <!-- OUR ADVANTAGES -->

    <section class="our-advantages">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 text-center">
                    <p class="advantages-label">No deposit required</p>
                    <hr class="advantages-line-hr">
                    <p class="advantages-infotext">You pay directly to the driver upon aaival at your destination.
                        Payment should be made in cash (Thai Baht only).</p>
                </div>
                <div class="col-sm-3 text-center">
                    <p class="advantages-label">No deposit required</p>
                    <hr class="advantages-line-hr">
                    <p class="advantages-infotext">You pay directly to the driver upon aaival at your destination.
                        Payment should be made in cash (Thai Baht only).</p>
                </div>
                <div class="col-sm-3 text-center">
                    <p class="advantages-label">Child car seats available</p>
                    <hr class="advantages-line-hr">
                    <p class="advantages-infotext">Child/baby car seats/boosters are availiable for any group for
                        additional 200 baht fee per seat.</p>
                </div>
                <div class="col-sm-3 text-center">
                    <p class="advantages-label">Pick up from any location</p>
                    <hr class="advantages-line-hr">
                    <p class="advantages-infotext">This is door-to-door private transfer service. We can pick you up at
                        airport, hotel or your own private address.</p>
                </div>
                <div class="col-sm-12 text-center">
                    <button class="more-info-button">MORE INFO</button>
                </div>
            </div>
        </div>
    </section>

    <!-- How to book -->

    <section class="how-to-book">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p class="label-how-to-book">How to book</p>
                    <p class="how-to-book-steps">1. Use <span style="text-decoration: underline;">Trip Calculator</span>
                        above to get for your trip or proceed dirextly to <a href="/reservation">Reservation
                            Form</a></p>
                    <p class="how-to-book-steps">2. Fill your Reservation Form and await reply from our reservation
                        team</p>
                    <p class="how-to-book-steps">3. Receive your booking confirmation letter containing full pick up
                        instructions and contact details</p>
                    <p class="how-to-book-steps">4. Meet your driver at appointed tim and enjoy your trip</p>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script type="text/javascript">
        $('.datePickerVehicles').datepicker({
            language: 'en',
            dateFormat: 'dd.mm.yyyy',
            timeFormat: 'hh:ii',
            minDate: new Date() // Now can select only dates, which goes after today
        });

        function getCosts() {
            var id_pick_up = $("#id_pick_up").val();
            var id_drop_off = $("#id_drop_off").val();
            var id_car = $("#id_car:checked").val();
            var date = $(".datePickerVehicles").val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'post',
                url: "{{route('ajax.routes_prices')}}",
                data: {
                    'id_pick_up': id_pick_up,
                    'id_drop_off': id_drop_off,
                    'id_car': id_car,
                    'date': date
                },
                success: function (response) {
                    setCarCost(response.cars, response.commission);
                    $("#mileage").text(response.mileage);
                    $("#travels_time").text(response.travels_time);

                },
                error: function (error) {
                    console.log(error)
                }

            });
        }
        function setCarCost(cars, commission) {
            $.each(cars, function (i, car) {
                $("#price_car_" + car.id_car).text(car.price + commission)
            });
        }

    </script>
@endsection
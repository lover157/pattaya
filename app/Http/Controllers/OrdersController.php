<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Client;
use App\ConfirmOrder;
use App\Location;
use App\Order;
use App\Car;
use App\Route;
use App\Status;
use App\Mail\OrderConfirm;

use Carbon\Carbon;
use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
use Input;
use const LOCK_EX;
use Validator;
use Illuminate\Support\Facades\Hash;
use Mail;
class OrdersController extends Controller
{

    private $paginationCount = 20;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $orders = new Order();
        $orders->orderBy('id', 'asc');
        $routes = Route::all()->keyBy('id')->toArray();
        $clients = Client::all()->keyBy('id')->toArray();
        $cars = Car::all()->keyBy('id')->toArray();
        $agents = Agent::all()->keyBy('id')->toArray();
        $locations = Location::all()->keyBy('id')->toArray();
        $statuses = Status::all()->keyBy('id')->toArray();
//        dd($request->input());

        $orders = $this->filterByLocations($orders, $request);
        $orders = $this->filterByDates($orders, $request);
        $orders = $this->filterByStatus($orders, $request);

        $orders = $orders->orderBy('id','desc')->paginate($this->paginationCount);

        return view('crm.orders.index', [
            'orders' => $orders,
            'routes' => $routes,
            'clients' => $clients,
            'cars' => $cars,
            'agents' => $agents,
            'locations' => $locations,
            'statuses' => $statuses
        ]);
    }

    private function filterByLocations($orders, $request)
    {

        if ($request->filter == 'y') {

            $location_from = $request->location_from;
            $location_to = $request->location_to;

            if ($location_from != $location_to) {

                if ($location_from == 0) {
                    $filteredRoutes = Route::where('id_drop_off', $location_to)->get()->keyBy('id')->keys();
                } elseif ($location_to == 0) {
                    $filteredRoutes = Route::where('id_pick_up', $location_from)->get()->keyBy('id')->keys();
                } else {
                    $filteredRoutes = Route::where('id_pick_up', $location_from)->where('id_drop_off', $location_to)->get()->keyby('id')->keys();
                }

                $orders = $orders->whereIn('id_route', $filteredRoutes);

            }
        }
        return $orders;

    }

    private function filterByDates($orders, $request)
    {
        if ($request->filter == 'y') {

            if ($request->date_from) {
                $date_from = Carbon::createFromFormat('d/m/Y', $request->date_from);
                $date_from->hour = 0;
                $date_from->minute = 0;
                $date_from->second = 0;
                $orders = $orders->where('date', '>', $date_from);

            }
            if ($request->date_to) {
                $date_to = Carbon::createFromFormat('d/m/Y', $request->date_to);
                $date_to->hour = 23;
                $date_to->minute = 59;
                $date_to->second = 59;
                $orders = $orders->where('date', '<', $date_to);
            }
        }
        return $orders;
    }

    private function filterByStatus($orders, $request)
    {
        if ($request->filter == 'y') {

            $status_id = $request->id_status;
            if ($status_id != 0) {
                $orders = $orders->where('id_status', $status_id);
            }

        }
        return $orders;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locations = Location::all();
        $cars = Car::all();
        $agents = Agent::all();
        $statuses = Status::all();
        $clients = Client::all();
        return view('crm.orders.create', ['locations' => $locations, 'cars' => $cars, 'agents' => $agents, 'statuses' => $statuses, 'clients' => $clients]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = new Order;

//        dd($request->date);
        if ($request->client_type == 'new_client') {
            $client = new Client();
            $client->name = $request->name;
            $client->email = $request->email;
            $client->phone = $request->phone;
            $client->save();
            $order->id_client = $client->id;
        } else {
            $order->id_client = $request->id_client;
        }

        $route = Route::where('id_pick_up', $request->id_pick_up)->where('id_drop_off', $request->id_drop_off)->first();


        $order->id_route = $route->id;
        $order->id_car = $request->id_car;
        $order->id_agent = $request->id_agent;
        $order->date = Carbon::createFromFormat('d.m.Y H:i', $request->date);


        if ($request->pick_up_address) $order->pick_up_address = $request->pick_up_address;
        if ($request->flight_num) $order->flight_num = $request->flight_num;
        if ($request->drop_off_address) $order->drop_off_address = $request->drop_off_address;
        if ($request->count_adults) $order->count_adults = $request->count_adults;
        if ($request->count_kids) $order->count_kids = $request->count_kids;
        if ($request->count_kids_seat) $order->count_kids_seat = $request->count_kids_seat;
        if ($request->seats_info) $order->seats_info = $request->seats_info;
        if ($request->count_luggage) $order->count_luggage = $request->count_luggage;
        if ($request->luggage_info) $order->luggage_info = $request->luggage_info;
        if ($request->vip) $order->vip = $request->vip;
        if ($request->commission) $order->commission = $request->commission;
        if ($request->total_price) $order->total_price = $request->total_price;
        if ($request->special_request) $order->special_request = $request->special_request;
        if ($request->id_status) $order->id_status = $request->id_status;
        $order->save();

        return redirect()->route('orders.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::findOrFail($id);
        $client = $order->client()->first();
        $car = $order->car()->first();
        $route = $order->route()->first();
        $locations = Location::all()->keyBy('id')->toArray();
        return view('crm.orders.show', ['order' => $order, 'client' => $client, 'car' => $car, 'route' => $route, 'locations' => $locations]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::findOrFail($id);
        $locations = Location::all();
        $cars = Car::all();
        $agents = Agent::all();
        $statuses = Status::all();
        $client = Client::findOrFail($order->id_client);
        $locations_ids = Route::find($order->id_route);
        return view('crm.orders.edit', ['order' => $order, 'locations' => $locations, 'cars' => $cars, 'agents' => $agents,
            'statuses' => $statuses, 'client' => $client, 'location_ids' => $locations_ids]);
    }

    public function copy($id)
    {
        $order = Order::findOrFail($id);
        $locations = Location::all();
        $cars = Car::all();
        $agents = Agent::all();
        $statuses = Status::all();
        $client = Client::findOrFail($order->id_client);
        $locations_ids = Route::find($order->id_route);
        return view('crm.orders.copy', ['order' => $order, 'locations' => $locations, 'cars' => $cars, 'agents' => $agents,
            'statuses' => $statuses, 'client' => $client, 'location_ids' => $locations_ids]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);

//        dd($request->date);

        $client = Client::find($order->id_client);
        $client->name = $request->name;
        $client->email = $request->email;
        $client->phone = $request->phone;
        $client->save();
        $order->id_client = $client->id;

        $route = Route::where('id_pick_up', $request->id_pick_up)->where('id_drop_off', $request->id_drop_off)->first();

        $order->id_route = $route->id;
        $order->id_car = $request->id_car;
        $order->id_agent = $request->id_agent;
        $order->date = Carbon::createFromFormat('d.m.Y H:i', $request->date);


        if ($request->pick_up_address) $order->pick_up_address = $request->pick_up_address;
        if ($request->flight_num) $order->flight_num = $request->flight_num;
        if ($request->drop_off_address) $order->drop_off_address = $request->drop_off_address;
        if ($request->count_adults) $order->count_adults = $request->count_adults;
        if ($request->count_kids) $order->count_kids = $request->count_kids;
        if ($request->count_kids_seat) $order->count_kids_seat = $request->count_kids_seat;
        if ($request->seats_info) $order->seats_info = $request->seats_info;
        if ($request->count_luggage) $order->count_luggage = $request->count_luggage;
        if ($request->luggage_info) $order->luggage_info = $request->luggage_info;
        if ($request->vip) $order->vip = $request->vip;
        if ($request->commission) $order->commission = $request->commission;
        if ($request->total_price) $order->total_price = $request->total_price;
        if ($request->special_request) $order->special_request = $request->special_request;
        if ($request->id_status) $order->id_status = $request->id_status;
        $order->save();

        return redirect()->route('orders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $order = Order::findOrFail($request->orderID);
        $order->delete();
        return redirect()->route('orders.index');
    }

    public function siteForm(Request $request)
    {
        $order = new Order;

        $name = $request->name;
        $email = $request->email;
        $phone = $request->phone;

        $client = Client::firstOrCreate(array('email'=>$email, 'name' => $name, 'phone'=>$phone));
        $order->id_client = $client->id;
        $route = Route::where('id_pick_up', $request->id_pick_up)->where('id_drop_off', $request->id_drop_off)->first();

        $order->id_route = $route->id;
        $order->id_car = $request->id_car;
        $order->id_agent = $request->id_agent;
        $order->date = Carbon::createFromFormat('d.m.Y H:i', $request->date);

        $location_from = Location::findOrFail($request->id_pick_up);
        if ($location_from->flight == 1) {
            if ($request->flight_num) $order->flight_num = $request->pick_up;
        } else {
            if ($request->pick_up_address) $order->pick_up_address = $request->pick_up;
        }
        if ($request->drop_off_address) $order->drop_off_address = $request->drop_off_address;
        if ($request->count_adults) $order->count_adults = $request->count_adults;
        if ($request->count_kids) $order->count_kids = $request->count_kids;
        if ($request->count_kids_seat) $order->count_kids_seat = $request->count_kids_seat;
        if ($request->seats_info) $order->seats_info = $request->seats_info;
        if ($request->count_luggage) $order->count_luggage = $request->count_luggage;
        if ($request->luggage_info) $order->luggage_info = $request->luggage_info;
        if ($request->vip) $order->vip = $request->vip;
        if ($request->commission) $order->commission = $request->commission;
        if ($request->total_price) $order->total_price = $request->total_price;
        if ($request->special_request) $order->special_request = $request->special_request;
        $order->id_status = 1;
        $order->save();

        $confirm = new ConfirmOrder();
        $confirm->hash = md5($order->id.$order->id_route.$order->total_price);
        $confirm->id_order = $order->id;
        $confirm->email = $email;
        $confirm->save();

//        Mail::to($email)->send(new OrderConfirm($confirm->hash));


        return response()->json([
            'ok' => 'ok',
            'id' => $order->id
        ]);

    }
}

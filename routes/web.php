<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/home', function () {
    return redirect('/crm');
});

Route::get('/', 'SiteController@vehicles')->name('vehicles');
Route::get('/reservation', 'SiteController@reservation')->name('reservation');

Route::middleware(['auth'])->group(function () {

    Route::prefix('crm')->group(function () {

        Route::get('/', 'CRMController@index')->name('crm');

        Route::prefix('orders')->group(function () {
            Route::get('/', 'OrdersController@index')->name('orders.index');
            Route::get('/create', 'OrdersController@create')->name('orders.create');
            Route::get('/show/{id}', 'OrdersController@show')->name('orders.show');
            Route::get('/edit/{id}', 'OrdersController@edit')->name('orders.edit');
            Route::get('/copy/{id}', 'OrdersController@copy')->name('orders.copy');

            Route::post('/store', 'OrdersController@store')->name('orders.store');
            Route::put('/update/{id}', 'OrdersController@update')->name('orders.update');
            Route::delete('/destroy/', 'OrdersController@destroy')->name('orders.destroy');

            Route::post('/siteForm/', 'OrdersController@siteForm')->name('orders.site.form');
        });

        Route::prefix('clients')->group(function () {
            Route::get('/', 'ClientsController@index')->name('clients.index');
            Route::get('/create', 'ClientsController@create')->name('clients.create');
            Route::get('/show/{id}', 'ClientsController@show')->name('clients.show');
            Route::get('/edit/{id}', 'ClientsController@edit')->name('clients.edit');

            Route::post('/store', 'ClientsController@store')->name('clients.store');
            Route::put('/update/{id}', 'ClientsController@update')->name('clients.update');
            Route::delete('/destroy/', 'ClientsController@destroy')->name('clients.destroy');
        });

        Route::prefix('cars')->group(function () {
            Route::get('/', 'CarsController@index')->name('cars.index');
            Route::get('/create', 'CarsController@create')->name('cars.create');
            Route::get('/show/{id}', 'CarsController@show')->name('cars.show');
            Route::get('/edit/{id}', 'CarsController@edit')->name('cars.edit');

            Route::post('/store', 'CarsController@store')->name('cars.store');
            Route::put('/update/{id}', 'CarsController@update')->name('cars.update');
            Route::delete('/destroy/', 'CarsController@destroy')->name('cars.destroy');
        });

        Route::prefix('locations')->group(function () {
            Route::get('/', 'LocationsController@index')->name('locations.index');
            Route::get('/create', 'LocationsController@create')->name('locations.create');
            Route::get('/show/{id}', 'LocationsController@show')->name('locations.show');
            Route::get('/edit/{id}', 'LocationsController@edit')->name('locations.edit');

            Route::post('/store', 'LocationsController@store')->name('locations.store');
            Route::put('/update/{id}', 'LocationsController@update')->name('locations.update');
            Route::delete('/destroy/', 'LocationsController@destroy')->name('locations.destroy');
        });

        Route::prefix('routes')->group(function () {
            Route::get('/', 'RoutesController@index')->name('routes.index');
            Route::get('/create', 'RoutesController@create')->name('routes.create');
            Route::get('/show/{id}', 'RoutesController@show')->name('routes.show');
            Route::get('/edit/{id}', 'RoutesController@edit')->name('routes.edit');

            Route::post('/store', 'RoutesController@store')->name('routes.store');
            Route::put('/update/{id}', 'RoutesController@update')->name('routes.update');
            Route::delete('/destroy/', 'RoutesController@destroy')->name('routes.destroy');
        });

        Route::prefix('agents')->group(function () {
            Route::get('/', 'AgentsController@index')->name('agents.index');
            Route::get('/create', 'AgentsController@create')->name('agents.create');
            Route::get('/show/{id}', 'AgentsController@show')->name('agents.show');
            Route::get('/edit/{id}', 'AgentsController@edit')->name('agents.edit');

            Route::post('/store', 'AgentsController@store')->name('agents.store');
            Route::put('/update/{id}', 'AgentsController@update')->name('agents.update');
            Route::delete('/destroy/', 'AgentsController@destroy')->name('agents.destroy');
        });
        Route::prefix('settings')->group(function () {
            Route::get('/', 'SettingsController@index')->name('settings.index');
            Route::put('/setKidSeatsCost', 'SettingsController@setKidSeatsCost')->name('settings.setKidSeatsCost');

            Route::get('/markup/create', 'MarkupsController@create')->name('settings.markup.create');
            Route::get('/markup/edit/{id}', 'MarkupsController@edit')->name('settings.markup.edit');
            Route::post('/markup/store', 'MarkupsController@store')->name('settings.markup.store');
            Route::put('/markup/update/{id}', 'MarkupsController@update')->name('settings.markup.update');
            Route::delete('/markup/delete', 'MarkupsController@destroy')->name('settings.markup.delete');

        });

    });
});
Route::post('crm/orders/siteForm/', 'OrdersController@siteForm')->name('orders.site.form');

Route::prefix('ajax')->group(function () {
    Route::post('/route-prices', 'CostsController@getRoutePrices')->name('ajax.routes_prices');

});
Route::get('/confirm/{hash}', 'ConfirmController@confirm')->name('confirm.order');

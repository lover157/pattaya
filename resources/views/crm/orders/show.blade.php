@extends('layouts.crm')
@section('pageTitle', 'Edit Order')

@section('styles')
    <link href="{{ asset('css/datepicker.min.css') }}" rel="stylesheet">
@endsection


@section('content')
    <div class="row">
        <div class="col-xl-12 col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Order <b>#{{$order->id}}</b>
                        <br/>
                        <br/>
                        <b>DATE:</b> {{$order->date}}
                    </h3>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-2">
                            <p><b>Name:</b></p>
                            <p><b>Email:</b></p>
                            <p><b>Phone:</b></p>
                        </div>
                        <div class="col-xs-10">
                            <p>{{$client->name}}</p>
                            <p>{{$client->email}}</p>
                            <p>{{$client->phone}}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-2">
                            <p><b>From:</b></p>
                        </div>
                        <div class="col-xs-10">
                            <p>{{$locations[$route['id_pick_up']]['name']}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <p><b>Pick up address:</b></p>
                        </div>
                        <div class="col-xs-10">
                            <p>{{$order->pick_up_address}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <p><b>Flight:</b></p>
                        </div>
                        <div class="col-xs-10">
                            <p>{{$order->flight_num}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <p><b>To:</b></p>
                        </div>
                        <div class="col-xs-10">
                            <p>{{$locations[$route['id_drop_off']]['name']}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <p><b>Drop off address:</b></p>
                        </div>
                        <div class="col-xs-10">
                            <p>{{$order->drop_off_address}}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-2">
                            <p><b>Adults:</b></p>
                        </div>
                        <div class="col-xs-10">
                            <p>{{$order->count_adults}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <p><b>Kids:</b></p>
                        </div>
                        <div class="col-xs-10">
                            <p>{{$order->count_kids}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <p><b>Kids seats:</b></p>
                        </div>
                        <div class="col-xs-10">
                            <p>{{$order->count_kids_seat}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <p><b>Seats info:</b></p>
                        </div>
                        <div class="col-xs-10">
                            <p>{{$order->seats_info}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <p><b>Count luggage:</b></p>
                        </div>
                        <div class="col-xs-10">
                            <p>{{$order->count_luggage}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <p><b>Luggage info:</b></p>
                        </div>
                        <div class="col-xs-10">
                            <p>{{$order->luggage_info}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <p><b>Car</b></p>
                        </div>
                        <div class="col-xs-10">
                            <p>{{$car->name}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <p><b>Commission</b></p>
                        </div>
                        <div class="col-xs-10">
                            <p>{{$order->commission}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <p><b>Total Price</b></p>
                        </div>
                        <div class="col-xs-10">
                            <p>{{$order->total_price}}</p>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>
    </div>
@endsection


@section('scripts')
    <script src="{{ asset('js/datepicker.min.js') }}"></script>

    <script type="text/javascript">

    </script>
@endsection

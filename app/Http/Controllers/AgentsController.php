<?php

namespace App\Http\Controllers;

use App\Agent;
use Illuminate\Http\Request;

class AgentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agents = Agent::all();
        return view('crm.agents.index', ['agents' => $agents]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('crm.agents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $agent = new Agent;
        $agent->name = $request->name;
        $agent->phone = $request->phone;
        $agent->info = $request->info;
        $agent->save();

        return redirect()->route('agents.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agent = Agent::findorFail($id);
        return view('crm.agents.edit', ['agent' => $agent]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agent = Agent::findorFail($id);
        $agent->name = $request->name;
        $agent->phone = $request->phone;
        $agent->info = $request->info;
        $agent->save();

        return redirect()->route('agents.index');
    }


    public function destroy(Request $request)
    {
        $agent = Agent::findOrFail($request->id);
        $agent->delete();

        return redirect()->route('agents.index');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Location;


use function redirect;

class LocationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = Location::all();
        return view('crm.locations.index', ['locations' => $locations]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('crm.locations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //TODO Validation Location store
        $location = new Location;
        $location->name = $request->name;
        $location->is_airport = 0;
        if ($request->is_airport) $location->is_airport = $request->is_airport;
        $location->save();

        return redirect()->route('locations.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $location = Location::findOrFail($id);
        return view('crm.locations.edit', ['location' => $location]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //TODO Validation Location update
        $location = Location::findOrFail($id);
        $location->name = $request->name;
        $location->is_airport = 0;
        if ($request->is_airport) $location->is_airport = $request->is_airport;
        $location->save();

        return redirect()->route('locations.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $location = Location::findOrFail($request->id);
        $location->delete();

        return redirect()->route('locations.index');
    }
}

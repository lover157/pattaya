@extends('layouts.crm')
@section('pageTitle', 'Settings')

@section('content')
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Settings:</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Name</th>
                            <th>Value</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($settings as $setting)
                            <tr>
                                <td>{{$setting->id}}</td>
                                <td>{{$setting->display_name}}</td>
                                <td>{{$setting->value}}</td>
                                <td> <a href="#" role="button" onclick="showKidsSeatsChangeForm()"><i class="fa fa-2x fa-pencil"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>id</th>
                            <th>Name</th>
                            <th>Value</th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>



<a href="{{route('settings.markup.create')}}" class="btn btn-success" style="margin-bottom: 10px">
    <i class="fa fa-plus" aria-hidden="true" title="Add new Route"></i> Add new Markup</a>
<div class="row">
    <div class="col-xs-12">

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Markups:</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>From Date</th>
                        <th>To Date</th>
                        <th>Cost</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($markups as $markup)
                    <tr>
                        <td>{{$markup->id}}</td>
                        <td>{{$markup->from_date}}</td>
                        <td>{{$markup->to_date}}</td>
                        <td>{{$markup->cost}}</td>
                        <td>
                            <a href="{{route('settings.markup.edit', ['id'=>$markup->id])}}" title="Edit">
                                <i class="fa fa-2x fa-pencil"></i>
                            </a>
                            <a href="#" role="button" onclick="confirmDeleteMarkup('{{$markup->id}}')" title="delete"><i class="fa fa-2x fa-trash"></i></a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>id</th>
                        <th>From Date</th>
                        <th>To Date</th>
                        <th>Cost</th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>

<div class="modal modal-default" id="deleteMarkup" tabindex="-1" role="dialog" aria-labelledby="modalDeleteMarkup"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Delete markup</h4>
            </div>
            <form role="form" action="{{route('settings.markup.delete')}}" method="POST"
                  enctype="multipart/form-data"
                  id="formUserDelete">
                <div class="modal-body">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <input type="hidden" value="" id="markupID" name="id">
                    <p>Confirm delete markup </p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn pull-left" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger">Delete</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

    <div class="modal modal-default" id="setKidsSeatsCost" tabindex="-1" role="dialog" aria-labelledby="modalsetKidsSeatsCost"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Change price for kids seats</h4>
                </div>
                <form role="form" action="{{route('settings.setKidSeatsCost')}}" method="POST"
                      enctype="multipart/form-data">
                    <div class="modal-body">
                        {{csrf_field()}}
                        {{method_field('PUT')}}
                        <div class="form-group">
                            <label for="price" class="col-md-2 cols-sm-12 control-label">Price</label>
                            <div class="col-md-7 col-sm-12">
                                <input type="text" name="price"  required
                                       value = "{{App\Settings::where('name', 'kidSeatsCost')->first()->value}}"
                                       class="form-control" placeholder="150">
                            </div>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn pull-left" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



@endsection


@section('scripts')
<script type="text/javascript">
    function confirmDeleteMarkup(id) {
        $("#markupID").val(id);
        $('#deleteMarkup').modal('show');
    }
    function showKidsSeatsChangeForm() {
        $('#setKidsSeatsCost').modal('show');
    }
</script>
@endsection

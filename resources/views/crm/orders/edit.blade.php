@extends('layouts.crm')
@section('pageTitle', 'Edit Order')

@section('styles')
    <link href="{{ asset('css/datepicker.min.css') }}" rel="stylesheet">
@endsection


@section('content')
    <div class="row">
        <div class="col-xl-12 col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Add new Order</h3>
                </div>
                <div class="row">
                    <form role="form" action="{{route('orders.update', ['id'=>$order->id])}}" method="POST"
                          enctype="multipart/form-data">
                        {{csrf_field()}}
                        {{method_field('PUT')}}

                        <div class="box-body">
                            <h4 class="box-title" style="margin-left: 15px">Client's info:</h4>


                            <div class="form-group @if($errors->has('name'))has-error @endif">
                                <label for="name" class="col-md-2 cols-sm-12 control-label">Name</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="name" id="name"
                                           value="{{$client->name}}"
                                           class="form-control" placeholder="John Doe" required>
                                    @foreach ($errors->get('name') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('email'))has-error @endif">
                                <label for="email" class="col-md-2 cols-sm-12 control-label">Email</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="email" name="email" id="email"
                                           value="{{$client->email}}"
                                           class="form-control" placeholder="JohnDoe@example.com" required>
                                    @foreach ($errors->get('email') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('phone'))has-error @endif">
                                <label for="phone" class="col-md-2 cols-sm-12 control-label">Phone</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="phone" id="phone"
                                           value="{{$client->phone}}"
                                           class="form-control" placeholder="+1 (123) 456-78-90" required>
                                    @foreach ($errors->get('phone') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('date'))has-error @endif">
                                <label for="date" class="col-md-2 cols-sm-12 control-label">Date</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="date" id="date"
                                           value="{{date("d.m.Y H:m", strtotime($order->date))}}"
                                           class="form-control" placeholder="dd/mm/yyyy" required data-timepicker="true"
                                           data-language='en'>
                                    @foreach ($errors->get('phone') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('count_adults'))has-error @endif">
                                <label for="count_adults" class="col-md-2 cols-sm-12 control-label">Adults</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="count_adults" id="count_adults"
                                           value="{{$order->count_adults}}"
                                           class="form-control" placeholder="2" required>
                                    @foreach ($errors->get('count_adults') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('count_kids'))has-error @endif">
                                <label for="count_kids" class="col-md-2 cols-sm-12 control-label">Kids</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="count_kids" id="count_kids"
                                           value="{{$order->count_kids}}"
                                           class="form-control" placeholder="2">
                                    @foreach ($errors->get('count_kids') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('count_kids_seat'))has-error @endif">
                                <label for="count_kids_seat" class="col-md-2 cols-sm-12 control-label">Kids
                                    seats</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="count_kids_seat" id="count_kids_seat"
                                           value="{{$order->count_kids_seat}}"
                                           class="form-control" placeholder="2">
                                    @foreach ($errors->get('count_kids_seat') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('seats_info'))has-error @endif">
                                <label for="seats_info" class="col-md-2 cols-sm-12 control-label">Seats info
                                    seats</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="seats_info" id="seats_info"
                                           value="{{$order->seats_info}}"
                                           class="form-control" placeholder="2">
                                    @foreach ($errors->get('seats_info') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('count_adults'))has-error @endif">
                                <label for="count_luggage" class="col-md-2 cols-sm-12 control-label">Luggage</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="count_luggage" id="count_luggage"
                                           value="{{$order->count_luggage}}"
                                           class="form-control" placeholder="2" required>
                                    @foreach ($errors->get('count_adults') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('count_adults'))has-error @endif">
                                <label for="luggage_info" class="col-md-2 cols-sm-12 control-label">Luggage info</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="luggage_info" id="luggage_info"
                                           value="{{$order->luggage_info}}"
                                           class="form-control" placeholder="2">
                                    @foreach ($errors->get('count_adults') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>


                            <div class="form-group @if($errors->has('seats_info'))has-error @endif">
                                <label for="special_request" class="col-md-2 cols-sm-12 control-label">Special request
                                    seats</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="special_request" id="special_request"
                                           value="{{$order->special_request}}"
                                           class="form-control" placeholder="2">
                                    @foreach ($errors->get('special_request') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>

                            <h4 class="box-title" style="margin-left: 15px">Locations details:</h4>

                            <div class="form-group @if($errors->has('date'))has-error @endif">
                                <label for="id_pick_up" class="col-md-2 cols-sm-12 control-label">PickUp
                                    location</label>
                                <div class="col-md-7 col-sm-12">
                                    <select class="form-control" name="id_pick_up" required id="id_pick_up">
                                        <option value="0">Choose location</option>
                                        @foreach($locations as $location)
                                            <option value="{{$location->id}}" @if($location->id == $location_ids->id_pick_up) selected @endif>{{$location->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('pick_up_address'))has-error @endif">
                                <label for="pick_up_address" class="col-md-2 cols-sm-12 control-label">Address</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="pick_up_address" id="pick_up_address"
                                           value="{{$order->pick_up_address}}"
                                           class="form-control">
                                    @foreach ($errors->get('pick_up_address') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('flight_num'))has-error @endif">
                                <label for="flight_num" class="col-md-2 cols-sm-12 control-label">Flight num</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="flight_num" id="flight_num"
                                           value="{{$order->flight_num}}"
                                           class="form-control">
                                    @foreach ($errors->get('flight_num') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('date'))has-error @endif">
                                <label for="id_drop_off" class="col-md-2 cols-sm-12 control-label">DropOff
                                    location</label>
                                <div class="col-md-7 col-sm-12">
                                    <select class="form-control" name="id_drop_off" required id="id_drop_off"
                                             >
                                        <option value="0">Choose location</option>
                                        @foreach($locations as $location)
                                            <option value="{{$location->id}}" @if($location->id == $location_ids->id_drop_off) selected @endif>{{$location->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('drop_off_address'))has-error @endif">
                                <label for="drop_off_address" class="col-md-2 cols-sm-12 control-label">Address</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="drop_off_address" id="drop_off_address"
                                           value="{{$order->drop_off_address}}"
                                           class="form-control">
                                    @foreach ($errors->get('drop_off_address') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>

                            <h4 class="box-title" style="margin-left: 15px">Car's info:</h4>
                            <div class="form-group @if($errors->has('id_car'))has-error @endif">
                                <label for="date" class="col-md-2 cols-sm-12 control-label">Vehicle info</label>
                                <div class="col-md-7 col-sm-12">
                                    <select class="form-control" name="id_car" required id="id_car"
                                             >
                                        <option value="0">Choose Vehicle</option>
                                        @foreach($cars as $car)
                                            <option value="{{$car->id}}" @if($car->id == $order->id_car) selected @endif>{{$car->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <h4 class="box-title" style="margin-left: 15px">Prices info: <button class="btn btn-default" onclick="getPrices(); return false;">Get Prices (auto)</button></h4>
                            <div class="form-group @if($errors->has('commission'))has-error @endif">
                                <label for="commission" class="col-md-2 cols-sm-12 control-label">Commission</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="commission" id="commission" required
                                           value="{{$order->commission}}"
                                           class="form-control">
                                    @foreach ($errors->get('commission') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('total_price'))has-error @endif">
                                <label for="total_price" class="col-md-2 cols-sm-12 control-label">Total Price</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="total_price" id="total_price" required
                                           value="{{$order->total_price}}"
                                           class="form-control">
                                    @foreach ($errors->get('total_price') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('id_car'))has-error @endif">
                                <label for="date" class="col-md-2 cols-sm-12 control-label">Agent</label>
                                <div class="col-md-7 col-sm-12">
                                    <select class="form-control" name="id_agent">
                                        <option value="">Choose Agent</option>
                                        @foreach($agents as $agent)
                                            <option value="{{$agent->id}}" @if($agent->id == $order->id_agent) selected @endif>{{$agent->name}}</option>
                                        @endforeach
                                    </select>
                                    @foreach ($errors->get('id_agent') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('id_status'))has-error @endif">
                                <label for="id_status" class="col-md-2 cols-sm-12 control-label">Status</label>
                                <div class="col-md-7 col-sm-12">
                                    <select class="form-control" name="id_status" id="id_status">
                                        <option value="">Choose Status</option>
                                        @foreach($statuses as $status)
                                            <option value="{{$status->id}}" @if($status->id == $order->id_status) selected @endif>{{$status->name}}</option>
                                        @endforeach
                                    </select>
                                    @foreach ($errors->get('id_status') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('id_car'))has-error @endif">
                                <label for="vip" class="col-md-2 cols-sm-12 control-label">VIP</label>
                                <div class="col-md-7 col-sm-12">
                                    <input class="checkbox" name="vip" type="checkbox" value="1" @if($order->vip) checked @endif>
                                </div>
                            </div>

                        </div>


                        <div class="box-footer" style="background: none">
                            <div class="form-group col-lg-2 col-lg-offset-7 col-sm-10 col-sm-offset-1 col-xs-12">
                                <button type="submit"
                                        class=" form-group btn input-block-level form-control btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('scripts')
    <script src="{{ asset('js/datepicker.min.js') }}"></script>
    <script src="{{ asset('js/i18n/datepicker.en.js') }}"></script>

    <script type="text/javascript">

        $('#date').datepicker({
            language: 'en',
            dateFormat: 'dd.mm.yyyy',
            timeFormat: 'hh:ii'
        });


        function getPrices() {
            checkNullable();
        }

        function checkNullable() {
            var id_pick_up = $("#id_pick_up").val();
            var id_drop_off = $("#id_drop_off").val();
            var id_car = $("#id_car").val();
            var date = $("#date").val();
            var count_kids_seat = $("#count_kids_seat").val();


            if (id_pick_up != 0 && id_drop_off != 0 && id_car != 0 && date!='') {
                getPricesForRoute(id_pick_up, id_drop_off, id_car, date, count_kids_seat)
            }
            else{
                $("#priceError").text('Check fields for filling')
            }
        }


        function getPricesForRoute(id_pick_up, id_drop_off, id_car, date, count_kids_seat) {
            if (id_pick_up != id_drop_off) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'post',
                    url: "{{route('ajax.routes_prices')}}",
                    data: {
                        'id_pick_up': id_pick_up,
                        'id_drop_off': id_drop_off,
                        'id_car': id_car,
                        'date': date,
                        'count_kids_seat': count_kids_seat
                    },
                    success: function (data) {
                        $("#priceError").text('');
                        $("#total_price").val(data.price);
                        $("#commission").val(data.commission);
                    }

                })
                ;
            }

        }
    </script>
@endsection

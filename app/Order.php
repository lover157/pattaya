<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    public function route()
    {
        return $this->hasOne('App\Route', 'id', 'id_route');
    }

    public function client()
    {
        return $this->hasOne('App\Client', 'id', 'id_client');
    }

    public function car()
    {
        return $this->hasOne('App\Car', 'id', 'id_car');
    }

    public function agent()
    {
        return $this->hasOne('App\Agent', 'id', 'id_agent');
    }


}

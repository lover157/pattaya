@extends('layouts.site')
@section('pageTitle', 'Vehicles')

@section('content')
    <!-- RESERVATION FORM -->
    <section class="section-reservation-form">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="label-reservation-form">Reservation form</p>
                    <form action="{{route('orders.site.form')}}" method="POST" id="reservationForm"
                          onsubmit="sendFormReservation(); return false;">
                        {{csrf_field()}}
                        <div class="left-reservtion-form">
                            <div class="left-reservtion-form-details">
                                <p>Contact details</p>
                                <input type="text" placeholder="full name" name="name" required>
                                <input type="email" placeholder="email address" name="email" required id="email">
                                <input type="email" placeholder="verify email address" name="confirm_email" required id="confirm_email" data-toggle="tooltip" data-placement="bottom" title="">
                                <input type="phone" placeholder="contact phone number" name="phone" required id="phone">
                                <p>please include your country international code</p>
                            </div>
                            <hr>
                            <div class="left-reservtion-form-passengers-luggage">
                                <p class="passengers-luggage-label">Passengers and luggage</p>
                                <div class="group-ad-ki-lu">
                                    <label for="">adults
                                        <br>
                                        <select name="count_adults" onchange="getCosts()">
                                            <option value="0" selected>0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                        </select>
                                    </label>
                                    <label for="">kids
                                        <br>
                                        <select name="count_kids" onchange="getCosts()">
                                            <option value="0" selected>0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                        </select>
                                    </label>
                                    <label for="">luggage
                                        <br>
                                        <select name="count_luggage" onchange="getCosts()">
                                            <option value="0" selected>0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                        </select>
                                    </label>
                                </div>

                                <p class="passengers-luggage-label2">Do you have oversized luggage?</p>
                                <p class="passengers-luggage-label3">golf bags, strollers, bycicles, watersport boards,
                                    pet crates* etc.</p>
                                <div class="oversized-luggage">
                                    <label>
                                        No <input type="radio" name="luggage" value="no">
                                    </label>
                                    <label>
                                        Yes <input type="radio" name="luggage" value="yes">
                                        <input type="text" name="luggage_info">
                                    </label>
                                </div>

                                <p class="passengers-luggage-label2">Do you require child/baby car seat?</p>
                                <p class="passengers-luggage-label3">child car seat are available for any age group
                                    (+{{App\Settings::where('name', 'kidSeatsCost')->first()->value}} baht per seat)</p>
                                <div class="child-seat">
                                    <label>
                                        No <input type="radio" name="child_seats" value="no">
                                    </label>
                                    <label>
                                        Yes <input type="radio" name="seats" value="yes">
                                        <select name="count_kids_seat" id="count_kids_seat" onchange="getCosts()">
                                            <option value="0" selected>0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                        </select>
                                    </label>
                                </div>
                                <p class="live-animals-not">* live animals without crates are not allowed</p>
                            </div>
                        </div>

                        <div class="middle-reservtion-form">
                            <div class="pick-up-detils">
                                <p>Pick up details</p>
                                <select name="id_pick_up" id="id_pick_up" onchange="getCosts()">
                                    <option value="0">Pick up location</option>
                                    @foreach($locations as $location)
                                        <option value="{{$location->id}}">{{$location->name}}</option>
                                    @endforeach
                                </select>
                                <br>
                                <input type="text" placeholder="Pick up address or flight num" name="pick_up">
                                <br>
                                <input class=" pickup-arrival-date datePickerVehicles" data-timepicker="true" placeholder="01.01.2018"
                                       name="date" required/>
                                <p class="pick-up-text">please provide your flight number, arrival date and time
                                    indicated in your airticket</p>
                            </div>

                            <hr>

                            <div class="drop-off-detils">
                                <p>Drop off details</p>

                                <select name="id_drop_off" id="id_drop_off" onchange="getCosts()">
                                    <option value="0">i am traveling to</option>
                                    @foreach($locations as $location)
                                        <option value="{{$location->id}}">{{$location->name}}</option>
                                    @endforeach
                                </select>
                                <input type="text" placeholder="Drop off location" name="drop_off_address">
                                <p class="drop-off-text">please include your hotel/condo/villa name if applicable</p>
                            </div>

                            <hr>
                            <div class="special-request">
                                <p>Special request</p>
                                <textarea placeholder="Any quastions or special request for this booking?"
                                          name="special_request"></textarea>
                            </div>
                        </div>


                        <div class="right-reservtion-form">
                            <p>Choose a vehicle?</p>
                            @foreach($cars as $car)
                                <label>
                                    <img src="upload/{{$car->image}}" alt=""> <br>
                                    <input type="radio" id="id_car" name="id_car" value="{{$car->id}}"
                                           onchange="getCosts()"> {{$car->name}} - <span
                                            id="car_price_{{$car->id}}"></span> baht
                                </label>
                            @endforeach
                        </div>

                        <div class="bottom-reservtion-form">

                            <div class="bottom-booking-terms-hidden">
                                <p class="bottom-reservtion-zagolvok">Booking terms</p>

                                <ul>
                                    <li>no deposit requried</li>
                                    <li>free cancelation</li>
                                    <li>all inclusive rate</li>
                                    <li>rayment to driver</li>
                                </ul>
                            </div>

                            <div class="total-cost-side">
                                <p class="bottom-reservtion-zagolvok">Total cost</p>
                                <table>
                                    <tr>
                                        <td>child seats</td>
                                        <td><span id="childs_seats_cost"></span> baht</td></tr>
                                    <tr>
                                        <td>peak season fee</td>
                                        <td><span id="markup"></span> baht</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>total cost</td>
                                        <td><span id="total_price"></span> baht</td>
                                        <input type="hidden" name="total_price" value="">
                                        <input type="hidden" name="commission" value="">
                                    </tr>
                                </table>
                            </div>

                            <div class="vertical-line-bot">
                                <table border="1px" width="1px" height="200px"
                                       style="background-color: #fff; margin-top: 20px;"></table>
                            </div>

                            <div class="bottom-booking-terms">
                                <p class="bottom-reservtion-zagolvok">Booking terms</p>

                                <ul>
                                    <li>no deposit requried</li>
                                    <li>free cancelation</li>
                                    <li>all inclusive rate</li>
                                    <li>payment to driver</li>
                                </ul>
                            </div>
                            <br>
                            <button type="submit">Send booking request</button>

                        </div>


                </div>
                </form>
            </div>
        </div>
        </div>
    </section>
    <div class="modal modal-default" id="modalSuccess" tabindex="-1" role="dialog" aria-labelledby="modalDeleteOrder"
         aria-hidden="true">
        <div class="modal-dialog orderSuccess">
            <div class="modal-content">
                <div class="modal-header background-black">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="color: #fff">×</span></button>
                    <h4 class="modal-title">Your order is created </h4>
                </div>

                <div class="modal-body background-orange">
                    <p>Please, confirm your mail !</p>
                </div>

                <div class="modal-footer background-black">
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $('.datePickerVehicles').datepicker({
            language: 'en',
            dateFormat: 'dd.mm.yyyy',
            timeFormat: 'hh:ii',
            minDate: new Date() // Now can select only dates, which goes after today
        });

        function sendFormReservation() {

            var validate = validateForm();
            if(!validate){
                return false;
            }


            var data = $("#reservationForm").serialize();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'post',
                url: "{{route('orders.site.form')}}",
                data: data,
                success: function (response) {
                    $('#modalSuccess').modal('show');
                },
                error: function (error) {
                    console.log(error)
                }

            })
        }

        function getCosts() {
            var id_pick_up = $("#id_pick_up").val();
            var id_drop_off = $("#id_drop_off").val();

            var id_car = $("#id_car:checked").val();
            var date = $(".datePickerVehicles").val();
            var count_kids_seat = $("#count_kids_seat").val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'post',
                url: "{{route('ajax.routes_prices')}}",
                data: {
                    'id_pick_up': id_pick_up,
                    'id_drop_off': id_drop_off,
                    'id_car': id_car,
                    'date': date,
                    'count_kids_seat': count_kids_seat
                },
                success: function (response) {
                    setCarCost(response.cars, response.commission);
                    $("#childs_seats_cost").text(response.kids_seats);
                    $("#markup").text(response.markup);
                    $("#total_price").text(response.price);
                    $("input[name=total_price]").val(response.price);
                    $("input[name=commission]").val(response.commission);
                },
                error: function (error) {
                    console.log(error)
                }

            });
        }

        function setCarCost(cars, commission) {
            $.each(cars, function (i, car) {
                $("#car_price_" + car.id_car).text(car.price + commission)
            });
        }

        function validateForm() {
            var email = $("#email").val();
            var confirm_email = $("#confirm_email").val();

            if(email != confirm_email){
                $('html, body').animate({
                    scrollTop: $("#email").offset().top
                }, 500);
                $("#confirm_email").focus();
                $("#confirm_email").attr('title', 'Value does not math email');
                return false;
            }

            return true;

        }
    </script>
@endsection
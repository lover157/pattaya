<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('pageTitle') - pattayacab</title>
    <!-- CSS -->
    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>

<body>

<header>
    <nav class="navbar navbar-default" role="navigation">
        <div class="container">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand logo" href="/"><img src="img/pattaya-cab.jpg" alt=""></a>
            </div>

            <div class="collapse navbar-collapse" id="main-menu">
                <ul class="nav navbar-nav">
                    <li class="{{isActiveRoute('vehicles', $output = "active")}}"><a href="{{route('vehicles')}}">vehicles</a></li>
                    <li class="{{isActiveRoute('reservation', $output = "active")}}"><a href="{{route('reservation')}}">reservation</a></li>
                    <li><a href="#">tours</a></li>
                    <li><a href="#">faq</a></li>
                    <li><a href="#">contact</a></li>
                </ul>

                <div class="partners-image">
                    <img src="img/facebook.jpg" alt="">
                    <img src="img/google.jpg" alt="">
                    <img src="img/trip-advisor.jpg" alt="">
                </div>
            </div>

        </div>
    </nav>
</header>

@yield('content')

<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <p>PATTAYA CAB PRIVATE TRANSFER SERVICES - 2018</p>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery -->
{{--<script src="{{ asset('js/jquery-2.2.3.min.js') }}"></script>--}}
<script src="https://code.jquery.com/jquery.js" type="text/javascript"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/datepicker.min.js') }}"></script>
<script src="{{ asset('js/i18n/datepicker.en.js') }}"></script>


<!-- Custom script -->
<script src="{{ asset('js/script.js') }}"></script>

@yield('scripts')


</body>

</html>
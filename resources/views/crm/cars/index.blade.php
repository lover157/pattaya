@extends('layouts.crm')
@section('pageTitle', 'Vehicles')

@section('content')
    <a href="{{route('cars.create')}}" class="btn btn-success" style="margin-bottom: 10px">
        <i class="fa fa-plus" aria-hidden="true" title="Создать группу"></i> Add new Car</a>
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Cars:</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>image</th>
                            <th>Name</th>
                            <th>About</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cars as $car)
                           <tr>
                               <td>{{$car->id}}</td>
                               <td>
                                   <img src="/public/upload/small_{{$car->image}}"/>
                               </td>
                               <td>{{$car->name}}</td>
                               <td>{{$car->about_1}} <br/>{{$car->about_2}}</td>
                               <td>
                                   <a href="{{route('cars.edit', ['id'=>$car->id])}}" title="Edit"><i class="fa fa-2x fa-pencil"></i></a>
                                   <a href="#" role="button" onclick="confirmDeleteCar('{{$car->id}}', '{{$car->name}}')" title="delete"><i class="fa fa-2x fa-trash"></i></a>
                               </td>
                           </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>id</th>
                            <th>image</th>
                            <th>Name</th>
                            <th>About</th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="modal modal-default" id="deleteCar" tabindex="-1" role="dialog" aria-labelledby="modalDeleteCar"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Delete vehicle</h4>
                </div>
                <form role="form" action="{{route('cars.destroy')}}" method="POST"
                      enctype="multipart/form-data"
                      id="formUserDelete">
                    <div class="modal-body">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                        <input type="hidden" value="" id="carID" name="id">
                        <p>Confirm delete car <span class="text-bold" id="carName"></span></p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn pull-left" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


@endsection


@section('scripts')
    <script type="text/javascript">
        function confirmDeleteCar(id, name) {
            $("#carName").text(name);
            $("#carID").val(id);
            $('#deleteCar').modal('show');
        }
    </script>
@endsection
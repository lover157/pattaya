<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('pageTitle') - CRM pattayacabcrm</title>
    <!-- Styles -->
    <!-- Theme style -->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link href="{{ asset('css/crm/AdminLTE.css') }}" rel="stylesheet">
    <link href="{{ asset('css/crm/themes.css') }}" rel="stylesheet">
    @yield('styles')


</head>

<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="/crm" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>C</b>RM</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Pattaya</b>cab</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="user user-menu">
                        <a href="#">
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        </a>
                    </li>
                    <li class="notifications-menu">
                        <a href="{{ route('logout') }}" role="button" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">
        <section class="sidebar">

            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
                <li class="{{ isActiveURL('/crm') }}">
                    <a href="/crm">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>
                <li class="{{ areActiveRoutes(['orders.*'], 'active')}}">
                    <a href="{{route('orders.index')}}">
                        <i class="fa fa-list"></i> <span>Orders</span>
                    </a>
                </li>
                <li class="{{ areActiveRoutes(['clients.*'], 'active')}}">
                    <a href="{{route('clients.index')}}">
                        <i class="fa fa-address-card"></i> <span>Clients</span>
                    </a>
                </li>
                <li class="{{ areActiveRoutes(['locations.*'], 'active')}}">
                    <a href="{{route('locations.index')}}">
                        <i class="fa fa-flag"></i> <span>Locations</span>
                    </a>
                </li>
                <li class="{{ areActiveRoutes(['routes.*'], 'active')}}">
                    <a href="{{route('routes.index')}}">
                        <i class="fa fa-road"></i> <span>Routes</span>
                    </a>
                </li>
                <li class="{{ areActiveRoutes(['cars.*'], 'active')}}">
                    <a href="{{route('cars.index')}}">
                        <i class="fa fa-automobile"></i> <span>Cars</span>
                    </a>
                </li>
                <li class="{{ areActiveRoutes(['agents.*'], 'active')}}">
                    <a href="{{route('agents.index')}}">
                        <i class="fa fa-male"></i> <span>Agents</span>
                    </a>
                </li>
                <li class="{{ areActiveRoutes(['settings.*'], 'active')}}">
                    {{--<a href="{{route('settings.index')}}">--}}
                    <a href="{{route('settings.index')}}">
                        <i class="fa fa-cogs"></i> <span>Settings</span>
                    </a>
                </li>

            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            @yield('content')
        </section>
        <!-- /.content -->
    </div>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
</div>
<!-- Scripts -->
<script src="{{ asset('js/jquery-2.2.3.min.js') }}"></script>
{{--<script src="{{ asset('js/datepicker.min.js') }}"></script>--}}
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script src="{{ asset('js/moment/moment-timezone.min.js') }}"></script>
<script src="{{ asset('js/moment/moment-timezone-with-data-2012-2022.js') }}"></script>
<script src="{{ asset('js/crm/app.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('js/crm/demo.js') }}"></script>
@yield('scripts')
</body>
</html>

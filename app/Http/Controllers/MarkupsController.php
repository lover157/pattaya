<?php

namespace App\Http\Controllers;

use App\Markup;
use Illuminate\Http\Request;
use Carbon\Carbon;

class MarkupsController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('crm.settings.markups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $from_date = Carbon::createFromFormat('d.m.Y', $request->from_date);
        $from_date->hour = 0;
        $from_date->minute = 0;
        $from_date->second = 0;

        $to_date = Carbon::createFromFormat('d.m.Y', $request->to_date);
        $to_date->hour = 23;
        $to_date->minute = 59;
        $to_date->second = 59;

        $markup = new Markup;
        $markup->from_date = $from_date;
        $markup->to_date = $to_date;
        $markup->cost = $request->cost;
        $markup->save();

        return redirect()->route('settings.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $markup = Markup::findorFail($id);

        return view('crm.settings.markups.edit', ['markup' => $markup]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $from_date = Carbon::createFromFormat('d.m.Y', $request->from_date);
        $from_date->hour = 0;
        $from_date->minute = 0;
        $from_date->second = 0;

        $to_date = Carbon::createFromFormat('d.m.Y', $request->to_date);
        $to_date->hour = 23;
        $to_date->minute = 59;
        $to_date->second = 59;

        $markup = Markup::findOrFail($id);
        $markup->from_date = $from_date;
        $markup->to_date = $to_date;
        $markup->cost = $request->cost;
        $markup->save();

        return redirect()->route('settings.index');
    }


    public function destroy(Request $request)
    {
        $markup = Markup::findOrFail($request->id);
        $markup->delete();

        return redirect()->route('settings.index');
    }
}

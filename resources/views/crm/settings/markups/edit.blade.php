@extends('layouts.crm')
@section('pageTitle', 'EditMarkup')


@section('styles')
    <link href="{{ asset('css/datepicker.min.css') }}" rel="stylesheet">
@endsection


@section('content')
    <div class="row">
        <div class="col-xl-12 col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Markup</h3>
                </div>
                <div class="row">
                    <form role="form" action="{{route('settings.markup.update', ['id'=>$markup->id])}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        {{method_field('PUT')}}

                        <div class="box-body">
                            <div class="form-group @if($errors->has('from_date'))has-error @endif">
                                <label for="from_date" class="col-md-2 cols-sm-12 control-label">From Date</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="from_date" id="from_date"
                                           value="{{date("d.m.Y", strtotime($markup->from_date))}}"
                                           class="form-control" placeholder="dd/mm/yyyy" required
                                           data-language='en'>
                                    @foreach ($errors->get('from_date') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('to_date'))has-error @endif">
                                <label for="to_date" class="col-md-2 cols-sm-12 control-label">To Date</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="to_date" id="to_date"
                                           value="{{date("d.m.Y", strtotime($markup->to_date))}}"
                                           class="form-control" placeholder="dd/mm/yyyy" required
                                           data-language='en'>
                                    @foreach ($errors->get('to_date') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('cost'))has-error @endif">
                                <label for="cost" class="col-md-2 cols-sm-12 control-label">Cost </label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="cost" id="cost"
                                           value="{{$markup->cost}}"
                                           class="form-control" placeholder="200" required>
                                    @foreach ($errors->get('cost') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>
                        </div>


                        <div class="box-footer" style="background: none">
                            <div class="form-group col-lg-2 col-lg-offset-7 col-sm-10 col-sm-offset-1 col-xs-12">
                                <button type="submit"
                                        class=" form-group btn input-block-level form-control btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('scripts')
    <script src="{{ asset('js/datepicker.min.js') }}"></script>
    <script src="{{ asset('js/i18n/datepicker.en.js') }}"></script>

    <script type="text/javascript">

        $('#from_date').datepicker({
            language: 'en',
            dateFormat: 'dd.mm.yyyy',
            timeFormat: 'hh:ii'
        });
        $('#to_date').datepicker({
            language: 'en',
            dateFormat: 'dd.mm.yyyy',
            timeFormat: 'hh:ii'
        });
    </script>
@endsection

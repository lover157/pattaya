<?php

namespace App\Http\Controllers;

use App\Price;
use App\Route;
use App\Markup;
use Carbon\Carbon;
use App\Settings;

use Illuminate\Http\Request;

class CostsController extends Controller
{
    public function getRoutePrices(Request $request)
    {

        $id_pick_up = $request->id_pick_up;
        $id_drop_off = $request->id_drop_off;
        $id_car = $request->id_car;
        $count_kid_seats = $request->count_kids_seat;


        $date = Carbon::createFromFormat('d.m.Y H:i', $request->date);

        $markup = 0;
        $markupData = Markup::where('from_date', '<=', $date)->where('to_date', '>=', $date)->first();
        if ($markupData) $markup = $markupData->cost;

        $kids_seats_cost = 0;

        if($count_kid_seats){
            $costCountSeat = Settings::where('name', 'kidSeatsCost')->first()->value;
            $kids_seats_cost = $count_kid_seats * $costCountSeat;
        }

        $route = $this->getRoute($id_pick_up, $id_drop_off);

        $price = Price::where('id_route', $route->id)->where('id_car', $id_car)->first();
        $cars = Price::where('id_route', $route->id)->get();

        return response()->json([
            'commission' => $route->commission,
            'markup' => $markup,
            'kids_seats' => $kids_seats_cost,
            'price' => $price->price + $markup + $kids_seats_cost + $route->commission,
            'cars' => $cars,
            'mileage' => $route->mileage,
            'travels_time' => $route->travels_time
        ]);
    }

    public function getRoute($id_pick_up, $id_drop_off)
    {
        $route = Route::where('id_pick_up', $id_pick_up)->where('id_drop_off', $id_drop_off)->first();
        return $route;
    }

}

<?php

namespace App\Http\Controllers;

use App\Location;
use App\Price;
use App\Route;
use App\Car;
use Illuminate\Http\Request;

class RoutesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $routes = Route::all();
        $locations = Location::all()->keyBy('id')->toarray();


        return view('crm.routes.index', ['routes' => $routes, 'locations' => $locations]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cars = Car::all();
        $locations = Location::all();
        return view('crm.routes.create', ['cars' => $cars, 'locations' => $locations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $route = new Route;
        $route->id_pick_up = $request->id_pick_up;
        $route->id_drop_off = $request->id_drop_off;
        $route->travels_time = $request->travels_time;
        $route->mileage = $request->mileage;
        $route->commission = $request->commission;
        $route->save();

        $data = $request->all();
        unset($data['id_pick_up'], $data['id_drop_off'], $data['_token'], $data['travels_time'], $data['mileage'], $data['commission'], $data['_url']);

        $cars = Car::all();
        foreach ($cars as $car) {
            $price = new Price;
            $price->id_route = $route->id;
            $price->id_car = $car->id;
            $price->price = $data['car_' . $car->id];
            $price->save();
        }

        return redirect()->route('routes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $route = Route::findOrFail($id);
        $locationFrom = $route->location_from()->get()->first();
        $locationTo = $route->location_to()->get()->first();
        $prices = $route->prices()->get()->keyBy('id')->toArray();
        $cars = Car::all()->keyBy('id')->toArray();

        return view('crm.routes.show',
            [
                'route' => $route,
                'locationFrom' => $locationFrom,
                'locationTo' => $locationTo,
                'prices' => $prices,
                'cars' => $cars,
            ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $route = Route::findOrFail($id);
        $cars = Car::all();
        $locations = Location::all();

        $prices = $route->prices()->get()->keyBy('id_car')->toArray();

        return view('crm.routes.edit', ['route' => $route, 'cars' => $cars, 'locations' => $locations, 'prices' => $prices]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $route = Route::findOrFail($id);
        $route->id_pick_up = $request->id_pick_up;
        $route->id_drop_off = $request->id_drop_off;
        $route->travels_time = $request->travels_time;
        $route->mileage = $request->mileage;
        $route->commission = $request->commission;
        $route->save();

        $data = $request->all();
        unset($data['id_pick_up'], $data['id_drop_off'], $data['_token'], $data['travels_time'], $data['mileage'], $data['commission'], $data['_url']);

        $cars = Car::all();

        foreach ($cars as $car) {
            $price = Price::where('id_route', $id)->where('id_car', $car->id)->first();
            if(!isset($price)){
                $price = new Price();
                $price->id_route = $id;
                $price->id_car = $car->id;
            }
            $price->price = $data['car_' . $car->id];
            $price->save();
        }

        return redirect()->route('routes.edit', ['id'=>$id]);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $route = Route::findOrFail($request->id);
        $route->delete();

        return redirect()->route('routes.index');
    }
}

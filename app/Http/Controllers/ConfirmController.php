<?php

namespace App\Http\Controllers;


use App\ConfirmOrder;
use App\Order;

use Illuminate\Http\Request;

class ConfirmController extends Controller
{
    public function confirm($hash)
    {
        $confirm = ConfirmOrder::where('hash', '=', $hash)->firstOrFail();
        $order = Order::findOrFail($confirm->id_order);
        $order->id_status = 2;
        $order->save();

        return view('site.confirm');
    }

}
